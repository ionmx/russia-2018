import Component from '@ember/component';
import { computed } from '@ember/object';
import { getScore } from '../utils/scoreUtils';


export default Component.extend({
  tagName: 'tr',

  homeGoalsDisplay: computed('match.homeGoals', function() {
   return (this.get('match.homeGoals') >= 0) ? this.get('match.homeGoals') : '-';
  }),

  visitorGoalsDisplay: computed('match.visitorGoals', function() {
   return (this.get('match.visitorGoals') >= 0) ? this.get('match.visitorGoals') : '-';
  }),

  prediction: computed('viewUser', 'viewUser.predictions', function() {
    var id = [this.get('viewUser.id'),'_',this.get('match.id')].join('');
    return this.get('viewUser.predictions').findBy('id', id);
  }),

  homePredictionDisplay: computed('prediction.homePrediction', function() {
    return (this.get('prediction.homePrediction') >= 0) ? this.get('prediction.homePrediction') : '-';
  }),

  visitorPredictionDisplay: computed('prediction.homePrediction', function() {
    return (this.get('prediction.visitorPrediction') >= 0) ? this.get('prediction.visitorPrediction') : '-';
  }),

  isEditable: computed('match', 'match.date', 'viewUser', 'session.currentUser', function() {
    var msBefore = 7200000;  // Two hours before
    var now = new Date().getTime();
    var matchDate = new Date(this.get('match.date')).getTime();

    return ((matchDate - now) > msBefore &&
              this.get('viewUser.id') == this.get('session.currentUser.uid'));
  }),

  userPoints: computed('viewUser', 'match.homeGoals', 'match.visitorGoals', 'prediction.homePrediction', 'prediction.visitorPrediction', function() {
    return getScore(this.get('match.homeGoals'),
                    this.get('match.visitorGoals'),
                    this.get('prediction.homePrediction'),
                    this.get('prediction.visitorPrediction'));
  }),

  actions: {
    updatePrediction: function(goals) {
      this.get('prediction').set('date', new Date().getTime());
      this.get('prediction').save().then(
        // Success
        function() {
          console.log('Prediction updated');
        },
        // Fail
        function() {
          alert('Error');
        }
      );
    }
  }

  
});