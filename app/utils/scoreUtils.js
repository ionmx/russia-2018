export function getOrdinal(n) {
   var s=["th","st","nd","rd"],
       v=n%100;
   return n+'<sup>'+(s[(v-20)%10]||s[v]||s[0])+'</sup>';
}

export function getWinner(home, visitor) {
  var winner = '';
  if (home > visitor) {
    winner = 'home';
  } else if (home < visitor) {
    winner = 'visitor';
  } else {
    winner = 'tied';
  }
  return winner;
}

export function getScore(home, visitor, homePrediction, visitorPrediction) {
  var points = 0;
  if (home >= 0 && homePrediction != null && visitorPrediction != null) {
    if ( (home == homePrediction) && (visitor == visitorPrediction) ) {
      points = 15;
    } else if (getWinner(home, visitor) == getWinner(homePrediction, visitorPrediction)) {
      points = 10 - Math.abs(homePrediction - home) - Math.abs(visitorPrediction - visitor);
      if (points < 0) { points = 0; }
    }
  }
  if (isNaN(points)) {
    points = 0;
  }
  return points;
}

export default { 
  getOrdinal: getOrdinal,
  getWinner: getWinner,
  getScore: getScore 
};